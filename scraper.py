import requests
from bs4 import BeautifulSoup


def get_product_links():
    # grab html containing voip gateway links
    product_endpoint = '/voip-gateway'
    r = requests.get(base_url + product_endpoint, headers={'User-Agent': 'Mozilla/5.0'})
    soup = BeautifulSoup(r.content, 'html.parser')

    # grab list of product links
    product_links = soup.find_all('b')
    list_of_product_links = []
    for link in product_links:
        if link.a:
            list_of_product_links.append(link.a.get('href'))

    return list_of_product_links


def scrape_products(scrape_url):
    r = requests.get(scrape_url, headers={'User-Agent': 'Mozilla/5.0'})
    soup = BeautifulSoup(r.content, 'html.parser')

    # grab list of products
    product_table = soup.find('a', attrs={'name': 'products'}).parent.table
    td_list = product_table.find_all('td')
    product_list = []
    description_list = []
    for td in td_list:
        if '<td align="left"' in str(td):
            if 'nowrap' in str(td) and len(td.next) > 2:
                product_list.append(str(td.next))
            elif '<font' not in str(td.next) and len(td.next) > 2:
                description_list.append(str(td.next))

    # grab overview and features
    try:
        overview_dict = {
            'overview': soup.find('span', class_='body_text').text,
            'features': soup.find('ul', class_='featurelist')
        }
    except AttributeError:
        print(f'No body text class span for this product: {scrape_url}')
        print('Trying div class col-lg-8 col-sm-6 col-xs-12')
        try:
            overview_dict = {
                'overview': soup.find('div', class_='col-lg-8 col-sm-6 col-xs-12').text,
                'features': soup.find('ul', class_='featurelist')
            }
        except AttributeError:
            print(f'No div class match for this product: {scrape_url}')
            overview_dict = {'overview': '', 'features': soup.find('ul', class_='featurelist')}

    new_product_list = [overview_dict]

    i = 0
    while i < len(product_list):
        product_dict = {
            'id': product_list[i],
            'description': description_list[i]
        }
        new_product_list.append(product_dict)
        i += 1

    for product in new_product_list:
        print(product)


if __name__ == '__main__':
    base_url = 'https://www.patton.com'
    print(get_product_links())
    product_url_list = get_product_links()

    for product_url in product_url_list:
        url = base_url + product_url
        scrape_products(url)
